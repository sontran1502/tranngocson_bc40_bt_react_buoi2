import React, { Component } from 'react'
import { glass } from './DataGlass'
export default class Playout extends Component {
  state = {
    "id": 1,
    price: 30,
    name: "GUCCI G8850U",
    url: "./glassesImage/v1.png",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
  }
  handleTestGlass = (item) => {
    let { url, price, name, desc } = item;
    this.setState({
      price: price,
      url: url,
      name: name,
      desc: desc,
    })
  }

  renderglass = () => {
    return glass.map((data) => {
      return (
      <div className='col-2'>
        <img onClick={() => { this.handleTestGlass(data) }} className=' w-100 h-100' src={data.url} alt="" />
      </div>
      )
  });


}


render() {
  let stylesPosition = {
    top: '2px',
    left: '2px',
    top: '106px',
    left: '95px',
    width: '194px',
    height: '55px',
    opacity: '0.8'

  }
  let background = {
    backgroundImage: 'url(./glassesImage/background.jpg)',
    backgroundRepeat: ' no-repeat',
    backgroundPosition: 'cover',
  }

  let content ={
    backgroundColor:'rgb(59, 145, 179)',
    opacity:'0.7',
    
  }


  return (
    <div className="container">
      <div style={background}>
        <div className="title p-2 text-center">
          <h3 >Try Glasses App Online</h3>
        </div>
        <div className="row justify-content-between position-relative">
          <img className='col-md-4' src="./glassesImage/model.jpg" alt="" />
          <img className='col-md-4' src="./glassesImage/model.jpg" alt="" />
          <img style={stylesPosition} className='position-absolute' src={this.state.url} alt="" />
          <div style={{
              bottom:"-16px",
            }}  className="content col-md-4 position-absolute text-center">
            <div style={content} className="">
            <h4 >{this.state.name}</h4>
            <p style={{
              fontSize:"28px",
              marginBottom:"0"
            }}>{this.state.price} $</p>
            <p>{this.state.desc}</p>
            </div>
          </div>
        </div>

        <div className=" mt-5 p-5 row">
          {this.renderglass()}
        </div>
      </div>
    </div>
  )
}
  }
